@testable import GarageWatch_Watch_App
import XCTest

class MockGarageApiClient: GarageApiClient {
    var getStatusCalled = false
    var toggleDoorCalled = false
    var getPingCalled = false

    override func getStatus() {
        getStatusCalled = true

        // Simulate the API response
        let doorStatus = DoorStatus(status: "closed", distance: 0, threshold: 0)
        delegate?.didCompleteStatusRequest(status: doorStatus)
    }

    override func toggleDoor() {
        toggleDoorCalled = true

        // Simulate the API response
        // You can perform additional assertions or validations here if needed
    }

    override func ping() {
        getPingCalled = true

        // Simulate the API response
        // You can perform additional assertions or validations here if needed
    }
}

class GarageApiClientTests: XCTestCase {
    var apiClient: MockGarageApiClient!
    var expectation: XCTestExpectation!

    override func setUp() {
        super.setUp()
        apiClient = MockGarageApiClient()
        expectation = XCTestExpectation(description: "API Request")
    }

    override func tearDown() {
        apiClient = nil
        expectation = nil
        super.tearDown()
    }

    func testGetStatus() {
        apiClient.delegate = self
        apiClient.getStatus()

        wait(for: [expectation], timeout: 0.1) // Reduced timeout value
    }

    func testToggleDoor() {
        apiClient.toggleDoor()

        // Perform assertions or validations based on the mock API response
        XCTAssertTrue(apiClient.toggleDoorCalled)
        // Additional assertions or validations

        expectation.fulfill()

        wait(for: [expectation], timeout: 0.1) // Reduced timeout value
    }

    func testGetPing() {
        apiClient.ping()

        // Perform assertions or validations based on the mock API response
        XCTAssertTrue(apiClient.getPingCalled)
        // Additional assertions or validations

        expectation.fulfill()

        wait(for: [expectation], timeout: 0.1) // Reduced timeout value
    }
}

extension GarageApiClientTests: GarageApiClientDelegate {
    func didCompleteStatusRequest(status _: DoorStatus) {
        // Perform assertions or validations based on the response

        // Fulfill the expectation to indicate the completion of the test
        expectation.fulfill()
    }
}
