import SwiftUI

@main
struct GarageWatch_Watch_AppApp: App {
    var body: some Scene {
        WindowGroup {
            GarageView()
        }
    }
}
