import Alamofire
import Foundation

class GarageApiClient {
    let toggleQueue = DispatchQueue(label: "com.code2app.garage-toggle", qos: .userInteractive, attributes: [.concurrent])
    var baseURL = "http://192.168.178.55"
    let requestTimeout: TimeInterval = 10
    let toggle = "/toggle"

    
    
    
    // The function is unaware of the door status. It will just push the trigger
    // of the garage door driver that will either open or close the door.
    func toggleDoor() {
        toggleQueue.async {
            AF.request(self.baseURL + self.toggle)
                .validate(statusCode: 200 ..< 300)
                .response { response in
                     debugPrint(response)
                }
        }
    }
}
