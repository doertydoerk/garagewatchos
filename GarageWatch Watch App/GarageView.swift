import SwiftUI
import WatchKit

struct GarageView: View {
    let activeButton = "Button"

    @State private var selection = 0
    @Environment(\.scenePhase) var scenePhase
    @ObservedObject var model: GarageViewModel

    var body: some View {
        VStack {
            Button {
                // nothing here, using long press instead of tap
            } label: {
                Image(activeButton)
            }
            .simultaneousGesture(LongPressGesture().onEnded { _ in
                buttonWasPushed()
            })
            .buttonStyle(PlainButtonStyle())

            .padding()            
        }
    }

    init() {
        model = GarageViewModel()
    }

    func buttonWasPushed() {
        model.toggleDoor()
        WKInterfaceDevice.current().play(.start)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        GarageView()
    }
}
