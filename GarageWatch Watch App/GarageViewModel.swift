import Foundation
import WatchKit

class GarageViewModel: ObservableObject {
    let apiClient = GarageApiClient()
    var timer: Timer?

    func toggleDoor() {
        apiClient.toggleDoor()
    }
}
